use std::io::{self, BufRead};
use std::str::FromStr;
use std::collections::HashSet;

fn main() {
    let stdin = io::stdin();
    let frequencies : Vec<i32> = stdin.lock().lines().map(|line_result| {
        i32::from_str(&line_result.unwrap()).unwrap()
    }).collect();
    let mut cycled_frequencies = frequencies.iter().cycle();
    let mut seen_frequencies = HashSet::new();
    let mut current_frequency = 0;

    cycled_frequencies.find(|frequency| {
        if seen_frequencies.contains(&current_frequency) {
            true
        } else {
            seen_frequencies.insert(current_frequency);
            current_frequency += *frequency;
            false
        }
    });

    println!("{}", current_frequency);
}
