extern crate itertools;
use itertools::Itertools;
use std::io::{self, BufRead};

fn main() {
    let mut pairs = 0;
    let mut triplets = 0;

    let stdin = io::stdin();
    let ids = stdin.lock().lines().map(|line_result| { line_result.unwrap() });

    ids.for_each(|id| {
        let mut pair_found = false;
        let mut triplet_found = false;

        for c in id.chars().unique() {
            let count = id.chars().filter(|other_c| { *other_c == c }).count();

            if count == 2 {
                pair_found = true
            } else if count == 3 { 
                triplet_found = true
            }

            if pair_found && triplet_found { break }
        }

        if pair_found { pairs += 1 }
        if triplet_found { triplets += 1 }
    });

    println!("{}", pairs * triplets);
}
