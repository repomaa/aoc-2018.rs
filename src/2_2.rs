use std::io::{self, BufRead};
use std::collections::HashSet;
use std::process;

fn main() {
    let stdin = io::stdin();
    let ids = stdin.lock().lines().map(|line_result| { line_result.unwrap() });
    let mut seen_fragments = HashSet::new();

    for id in ids {
        let char_count = id.chars().count();

        for index in 0..char_count {
            let fragment : String = id.chars()
                .zip(0..char_count)
                .filter(|(_, current_index)| { *current_index != index })
                .map(|(c, _)| { c })
                .collect();

            let fragment_with_index = (fragment.clone(), index);

            if !seen_fragments.insert(fragment_with_index) {
                println!("{}", fragment);
                process::exit(0);
            }
        }
    }
}
