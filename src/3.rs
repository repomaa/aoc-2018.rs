extern crate regex;
extern crate itertools;
use std::str::FromStr;
use std::io::{self, BufRead};
use std::ops::Range;
use std::num::ParseIntError;
use std::collections::HashSet;
use regex::Regex;
use itertools::Itertools;

trait Intersectable {
    fn intersection(&self, other: &Self) -> Self;
}

impl Intersectable for Range<i32> {
    fn intersection(&self, other: &Self) -> Self {
        self.start.max(other.start)..self.end.min(other.end)
    }
}

#[derive(PartialEq,Eq,Hash)]
struct Square {
    x: i32,
    y: i32
}

#[derive(PartialEq,Eq,Hash,Clone)]
struct Claim {
    id: i32,
    x: i32,
    y: i32,
    width: i32,
    height: i32
}

impl Claim {
    fn x_coords(&self) -> Range<i32> {
        self.x..(self.x + self.width)
    }

    fn y_coords(&self) -> Range<i32> {
        self.y..(self.y + self.height)
    }

    fn overlapping_squares(&self, other: &Self) -> Vec<Square> {
        let intersecting_x_coords = self.x_coords().intersection(&other.x_coords());
        let intersecting_y_coords = self.y_coords().intersection(&other.y_coords());
        if
            intersecting_x_coords.start > intersecting_x_coords.end ||
            intersecting_y_coords.start > intersecting_y_coords.end
        { return Vec::new() }

        intersecting_x_coords.cartesian_product(intersecting_y_coords)
            .map(|(x, y)| { Square { x: x, y: y } })
            .collect()
    }
}

impl FromStr for Claim {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let matcher = Regex::new(r"^#(?P<id>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<width>\d+)x(?P<height>\d+)$").unwrap();
        let capture_groups = matcher.captures(s).unwrap();

        Ok(
            Claim {
                id: i32::from_str(&capture_groups["id"])?,
                x: i32::from_str(&capture_groups["x"])?,
                y: i32::from_str(&capture_groups["y"])?,
                width: i32::from_str(&capture_groups["width"])?,
                height: i32::from_str(&capture_groups["height"])?
            }
        )
    }
}

fn main() {
    let stdin = io::stdin();
    let claims: HashSet<Claim> = stdin.lock().lines().map(|line_result| {
        Claim::from_str(&line_result.unwrap()).unwrap()
    }).collect();

    let mut non_overlapping_claims = claims.clone();
    let mut overlapping_squares = HashSet::new();

    for (claim_a, claim_b) in claims.clone().iter().tuple_combinations() {
        let mut overlapped = false;
        for square in claim_a.overlapping_squares(&claim_b) {
            overlapping_squares.insert(square);
            overlapped = true;
        }

        if overlapped {
            non_overlapping_claims.remove(claim_a);
            non_overlapping_claims.remove(claim_b);
        }
    }

    println!("Part 1: {}", overlapping_squares.len());
    println!("Part 2: {}", non_overlapping_claims.iter().next().unwrap().id);
}
